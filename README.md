
- si vous êtes user : installez l'extension sur votre navigateur (desktop ou mobile - Firefox en premier)
- si vous avez un site : ajoutez le script
- ajouter des fonctionnalités aux interfaces
- inscrivez-vous à des programmes (channels) d'essais de nouvelles fonctionnalités
- recevez des versions A/B des interfaces & des parcours utilisateurs
- choisissez de laisser ou non les stats d'usage anonymisées
- donnez du feedback explicite
- décrivez/proposez de nouvelles fonctionnalités

Dashboard :
- une appli/webextension pour suivre les stats des essais de fonctionnalités de manière transversale entre les plateformes

Pour les développeur·ses :
- développez de nouvelles fonctionnalités à proposer au cours d'une campagne


Campaigns d'ajout de fonctionnalités :
- ajoute une fonctionnalité de dataviz sur le sujet sur cette page
- permet de proposer une dataviz sur le sujet d'une page